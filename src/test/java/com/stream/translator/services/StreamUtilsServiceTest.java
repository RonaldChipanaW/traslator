package com.stream.translator.services;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class StreamUtilsServiceTest {
    @Autowired
    private StreamUtilsService streamUtilsService;

    @Test
    void copyReverseOrder()throws IOException {
        String inputFileName = "src/test/resources/original.txt";
        String outputFileName = "src/test/resources/original_reverse.txt";
        File outputFile = new File(outputFileName);
        InputStream in = new FileInputStream(inputFileName);
        OutputStream out = new FileOutputStream("src/test/resources/original_reverse.txt");
        String str = streamUtilsService.getStringFromInputStream(in);
        streamUtilsService.copiarEstrofasOrdenInverso(str,out);

    }
    @Test
    void contarEstrofas() throws IOException{
        String inputFileName = "src/test/resources/original_reverse.txt";
        InputStream in = new FileInputStream(inputFileName);
        String str = streamUtilsService.getStringFromInputStream(in);
        int count = streamUtilsService.contarEstrofas(str);
        assertEquals(16,count);
    }

    @Test
    void getPalabraMasRepetida()throws IOException {
        String inputFileName = "src/test/resources/original.txt";
        String outputFileName = "src/test/resources/statics.txt";
        File outputFile = new File(outputFileName);
        InputStream in = new FileInputStream(inputFileName);
        OutputStream out = new FileOutputStream("src/test/resources/statics.txt");
        String str = streamUtilsService.getStringFromInputStream(in);
        streamUtilsService.getPalabraMasRepetida(str,out);

    }

    @Test
    void remplazarPalabra()throws IOException {
        String inputFileName = "src/test/resources/original.txt";
        String outputFileName = "src/test/resources/final_output.txt";
        File outputFile = new File(outputFileName);
        InputStream in = new FileInputStream(inputFileName);
        OutputStream out = new FileOutputStream("src/test/resources/final_output.txt");
        String songFileName = "src/test/resources/original_reverse.txt";
        InputStream inSong = new FileInputStream(songFileName);
        String song = streamUtilsService.getStringFromInputStream(inSong);
        streamUtilsService.remplazarPalabra(in, song, out);

    }
}