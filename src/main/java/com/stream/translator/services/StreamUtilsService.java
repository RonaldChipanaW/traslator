package com.stream.translator.services;


import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

@Service
public class StreamUtilsService {
    public void copiarEstrofasOrdenInverso(String str, OutputStream out) throws IOException {
        String reverse = reverseOrder(str);
        StreamUtils.copy(reverse, StandardCharsets.UTF_8,out);

    }
    public String reverseOrder(String str){
        StringBuilder stanzas=new StringBuilder();
        StringBuilder song = new StringBuilder() ;
        Scanner in = new Scanner(str);
        while (in.hasNextLine()){
            String line = in.nextLine();
            if(line.equals("")){
                stanzas.append("\n");
                song.insert(0,stanzas.toString());
                stanzas= new StringBuilder();
            }else{
                stanzas.append(line+"\n");
            }
        }
        stanzas.append("\n");
        song.insert(0,stanzas.toString());

        return song.toString();
    }

    public String getStringFromInputStream(InputStream input)throws IOException{
        StringWriter writer = new StringWriter();
        IOUtils.copy(input, writer,"UTF-8");
        return writer.toString();
    }
    public int contarEstrofas(String str){
        int count=0;
        Scanner in = new Scanner(str);
        while(in.hasNextLine()){
            String line = in.nextLine();
            if(line.equals("")){
                count++;
            }
        }
        return count;
    }

    public void getPalabraMasRepetida(String str, OutputStream out) throws IOException{
        Map<String, Integer> ocurrencias= new HashMap<>();
        String[] SplitWords = str.split("[\\s,]+");
        for(String Word : SplitWords){
            Integer oldCount=ocurrencias.get(Word);
            if(oldCount == null){
                oldCount = 0;
            }
            ocurrencias.put(Word,oldCount + 1);
        }
        Integer value= ocurrencias.entrySet().stream().max((entry1, entry2) -> entry1.getValue() - entry2.getValue()).get().getValue();
        String key = ocurrencias.entrySet().stream().max((entry1, entry2) -> entry1.getValue() - entry2.getValue()).get().getKey();
        String static_str= "Word: "+ key +" ocurrencias: "+value;
        StreamUtils.copy(static_str, StandardCharsets.UTF_8, out);
    }

    public void remplazarPalabra(InputStream in, String song, OutputStream out) throws IOException {
            String str = getStringFromInputStream(in);
            String[] splitWords = str.split("[\\s,]+");
            String word= splitWords[1];
            String song_remplace = song.replace(word,"you");
            song_remplace = song.replace("'"," ");
            StreamUtils.copy(song_remplace, StandardCharsets.UTF_8, out);
    }


}

